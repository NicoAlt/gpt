# Gameplay Timer

[![F-Droid](https://f-droid.org/wiki/images/0/06/F-Droid-button_get-it-on.png)](https://f-droid.org/repository/browse/?fdid=de.nico.gpt)

Mainly developed by Nico Alt.

## What is this project for?

With Gameplay Timer you will never miss an event in your favourite game
again.

## How do I get set up?

A:
- *Import* the project in Android Studio.

B:

	gradle build

## TODO

Take a look at [GitLab Issues](https://gitlab.com/NicoAlt/gpt/issues).

## LICENSE

See [LICENSE](./LICENSE).
