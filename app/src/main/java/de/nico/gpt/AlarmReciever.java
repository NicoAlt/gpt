package de.nico.gpt;

/*
 * @author Nico Alt
 * See the file "LICENSE" for the full license governing this code.
 */

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import java.util.Random;

public class AlarmReciever extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        String what = intent.getStringExtra("what");
        String title = intent.getStringExtra("title");
        showNotification(context, title, what);
    }

    @SuppressWarnings("deprecation")
    // the else part
    private void showNotification(Context context, String title, String what) {
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            Uri soundUri = RingtoneManager
                    .getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
                    new Intent(context, MainGameActivity.class), 0);

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
                    context).setSound(soundUri)
                    .setLights(0xffcc0000, 1000, 100).setAutoCancel(true)
                    .setSmallIcon(R.drawable.ic_notification)
                    .setContentInfo("GPT").setContentTitle(title)
                    .setContentText(what).setTicker(what);
            mBuilder.setContentIntent(contentIntent);
            mBuilder.setDefaults(Notification.DEFAULT_SOUND);
            mBuilder.setAutoCancel(true);
            NotificationManager mNotificationManager = (NotificationManager) context
                    .getSystemService(Context.NOTIFICATION_SERVICE);

            Random generator = new Random();
            int id = generator.nextInt(564651684);
            mNotificationManager.notify(id, mBuilder.build());
        } else {
            NotificationManager mNotificationManager = (NotificationManager) context
                    .getSystemService(Context.NOTIFICATION_SERVICE);

            android.app.Notification notification = new android.app.Notification(
                    R.drawable.ic_notification, title,

                    System.currentTimeMillis());

            Intent notificationIntent = new Intent(context,
                    MainGameActivity.class);

            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,
                    notificationIntent, 0);

            notification
                    .setLatestEventInfo(context, title, what, pendingIntent);

            notification.vibrate = new long[]{100, 200, 100, 500};

            Random generator = new Random();
            int id = generator.nextInt(564651684);
            mNotificationManager.notify(id, notification);
        }
    }

}
